cmake_minimum_required(VERSION 3.13)
include(../../pico_sdk_import.cmake)
project(lab02 CXX C ASM)
pico_sdk_init()

# Specify the name of the executable.
add_executable(lab02
	lab02.c
)

# Specify the source files to be compiled.
#target_sources(lab01 PRIVATE lab01.c)

# Pull in commonly used features.
target_link_libraries(lab02 pico_stdlib)

# Create map/bin/hex file etc.
pico_add_extra_outputs(lab02)

# Add the URL via pico_set_program_url.
#apps_auto_set_url(lab01)
