#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.


/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * @fn double wallis(int n)
 * @fn main
 * @param (MAX) is the number of iterations
 * @param (PY) is value of PI
 * @param (IHAVETHEBIGGESTPIE) is our value of PI
 * @param (error) is percentage of error
 * @return int      Returns exit-status zero on completion.
 */

double wallisd(int n){  
  double MYBIGPIE = 1; //initialising MYBIGPIE as 1
  double left;         //calling left and right for use in for loop
  double right;
  for(double i = 1; i <= n; i++){
    left = (2 * i)/(2 * i -1);
    right = (2 * i)/(2 * i + 1);
    MYBIGPIE = MYBIGPIE * (left * right);
  }
  
  return 2 * MYBIGPIE;  //return wallis pi value
}

float wallisf(int n){  
  float MYBIGPIE = 1; //initialising MYBIGPIE as 1
  float left;         //calling left and right for use in for loop
  float right;
  for(float i = 1; i <= n; i++){
    left = (2 * i)/(2 * i -1);
    right = (2 * i)/(2 * i + 1);
    MYBIGPIE = MYBIGPIE * (left * right);
  }
  
  return 2 * MYBIGPIE;  //return wallis pi value
}

int main() {
  int MAX = 100000;
  double PY = 3.14159265359;
  double IHAVETHEBIGGESTPIE = wallisd(MAX);
  float IHAVEPIE = wallisf(MAX);
  double error = fabs((IHAVETHEBIGGESTPIE - PY)/PY) * 100;
  float error2 = fabs((IHAVEPIE - PY)/PY) * 100;
  printf("wallis double pi = %.11f. pi is %.11f . Percentage of error is %f percent.\n", IHAVETHEBIGGESTPIE, PY, error);
  printf("wallis double pi = %.11f. pi is %.11f . Percentage of error is %f percent.\n", IHAVEPIE, PY, error2);
    // printing out our value of pi, the accepted value of pi and the percetage of error. 

    // Returning zero indicates everything went okay.
    return 0;
}
