#include "pico/stdlib.h"

/**
 * @brief EXAMPLE - BLINK_C
 *        changed sample code to include a function outside man
 *        function is called in main and initiates the forever loop 
 *        to allow the LED to blink 
 * @return int  Application return code (zero for success).
 */
int subroutine(int pin_no, int length){ //function contains two parameters that can be changed or set while calling function
  while(true){                          //loop runs forever, there is no exit
    gpio_put(pin_no, 1);
    sleep_ms(length);
    gpio_put(pin_no, 0);
    sleep_ms(length);
  }
}

int main() {
  const uint LED_PIN = 25;              // Pin no is set to the LED's 
  gpio_init(LED_PIN);                   
  gpio_set_dir(LED_PIN, GPIO_OUT);
  subroutine(LED_PIN, 500);             //sleep time is set to 500, half a second

  return 0;                             //will never reach due to a forever loop in called function

}

